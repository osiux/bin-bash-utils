#!/bin/bash

# This script comes with ABSOLUTELY NO WARRANTY, use at own risk
# Copyright (C) 2010 Osiris Alejandro Gomez <osiux@osiux.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

[[ -n "$DIR" ]] || DIR="$HOME/bin"
cd "$DIR"       || exit 1

git ls-files --deleted -z | while read -r i
do
  git rm -f "$i" >/dev/null 2>&1
done

git status --porcelain | awk '{print $2}' | while read -r i
do
  git add "$i"
done

TMP="$(mktemp)"

[[ -n "$MSG" ]] || MSG=$(cat << EOF
autocommit

  @ 00:00 hs
EOF
)

echo "$MSG" > "$TMP" && git commit -F "$TMP" && git push origin --all

rm -f "$TMP"
